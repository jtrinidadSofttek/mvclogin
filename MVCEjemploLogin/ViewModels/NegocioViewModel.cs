﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI_MVCNegocios.ViewModels
{
    public class NegocioViewModel
    {
        //Propiedades
        public int Id { get; set; }

        [Required]
        [MinLength(2, ErrorMessage ="El nombre ingresado es demasiado corto.")]
        [MaxLength(40, ErrorMessage ="El nombre ingresado es demasiado largo.")]
        public string Nombre { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "La direccion ingresada es demasiada corta.")]
        [MaxLength(60, ErrorMessage = "La direccion ingresada es demasiada larga.")]
        public string Direccion { get; set; }
        public virtual ICollection<Empleado> Empleado { get; set; }

        //Constructor
        public NegocioViewModel(Models.Negocio negocio)
        {
            Empleado = negocio.Empleado;
            Id = negocio.Id;
            Nombre = negocio.Nombre;
            Email = negocio.Email;
            Direccion = negocio.Direccion;
        }

        public NegocioViewModel() { }
        //Metodos

        public Models.Negocio aNegocio()
        {
            Models.Negocio n = new Models.Negocio();
            n.Empleado = Empleado;
            n.Id = Id;
            n.Nombre = Nombre;
            n.Email = Email;
            n.Direccion = Direccion;

            return n;
        }
    }
}