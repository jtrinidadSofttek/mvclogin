namespace MVCEjemploLogin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "isNegocio");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "isNegocio", c => c.Boolean(nullable: false));
        }
    }
}
