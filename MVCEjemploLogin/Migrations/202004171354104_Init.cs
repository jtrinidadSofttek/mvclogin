namespace MVCEjemploLogin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Nombre", c => c.String());
            AddColumn("dbo.AspNetUsers", "isEmpleado", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "isNegocio", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "isNegocio");
            DropColumn("dbo.AspNetUsers", "isEmpleado");
            DropColumn("dbo.AspNetUsers", "Nombre");
        }
    }
}
