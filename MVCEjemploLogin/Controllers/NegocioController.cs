﻿
using System.Collections.Generic;
using System.Web.Mvc;
using NegocioBusiness;
using System.Net;
using UI_MVCNegocios.ViewModels;
using Models;
using System;
using System.Linq;

namespace MVCEjemploLogin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NegocioController : Controller
    {
        ABMNegocio db = new ABMNegocio();

        // GET: Negocio
        [AllowAnonymous]
        public ActionResult Index(string searchString)
        {
            List<NegocioViewModel> negocios = new List<NegocioViewModel>(); 
            db.GetNegocios().ForEach(x => negocios.Add(new NegocioViewModel(x)));

            if (!String.IsNullOrEmpty(searchString))
            {
                negocios = negocios.Where(n => n.Nombre.Contains(searchString)).ToList();
                                       
            }

            return View(negocios);
        }

        // GET: Negocio/Details/5
        public ActionResult Details(int id)
        {
            return View(new NegocioViewModel(db.GetNegocio(id)));
        }

        // GET: Negocio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Negocio/Create
        [HttpPost]
        public ActionResult Create(NegocioViewModel negocio)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var n = negocio.aNegocio();
                    db.AddNegocio(n);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(negocio);
            }
        }

        // GET: Negocio/Edit/5
        public ActionResult Edit(int id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var negocio = db.GetNegocio(id);
            if(negocio == null)
            {
                return HttpNotFound();
            }
            return View(new NegocioViewModel(negocio));
        }

        // POST: Negocio/Edit/5
        [HttpPost]
        public ActionResult Edit(NegocioViewModel negocio)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var n = negocio.aNegocio();
                    db.UpdateNegocio(n);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(negocio);
            }
        }

        // GET: Negocio/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var negocio = db.GetNegocio(id);
            if (negocio == null)
            {
                return HttpNotFound();
            }
            return View(new NegocioViewModel(negocio));
        }

        // POST: Negocio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var negocio = db.GetNegocio(id);
           db.RemoveNegocio(negocio);
           return RedirectToAction("Index");
                
        }
        //GET: Negocio/EmpleadosPorNegocio/5
        public ActionResult EmpleadosPorNegocio(int id)
        {
            List<EmpleadoViewModel> vista = new List<EmpleadoViewModel>();
            db.FilterNegocio(id).ForEach(x => vista.Add(new EmpleadoViewModel(x)));
            return View(vista);
        }
    }
}
