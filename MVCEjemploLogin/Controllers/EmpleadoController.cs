﻿using Models;
using NegocioBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UI_MVCNegocios.ViewModels;

namespace MVCEjemploLogin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmpleadoController : Controller
    {
        ABMEmpleado db = new ABMEmpleado();
        ABMNegocio dbN = new ABMNegocio();
        // GET: Empleado
        [AllowAnonymous]
        public ActionResult Index(String searchString)
        {
            List<EmpleadoViewModel> empleados = new List<EmpleadoViewModel>();
            db.GetEmpleados().ForEach(x => empleados.Add(new EmpleadoViewModel(x)));

            if (!String.IsNullOrEmpty(searchString))
            {
               empleados = empleados.Where(e => e.Nombre.Contains(searchString)).ToList();

            }

            return View(empleados);
        }

        // GET: Empleado/Details/5
        public ActionResult Details(int id)
        {
            return View(new EmpleadoViewModel(db.GetEmpleado(id)));
        }

        // GET: Empleado/Create
        public ActionResult Create()
        {
            ViewBag.Id_Negocio = new SelectList(dbN.GetNegocios(), "Id", "Nombre");
            return View();
        }

        // POST: Empleado/Create
        [HttpPost]
        public ActionResult Create(EmpleadoViewModel empleado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Empleado e = empleado.aEmpleado();
                    db.AddEmpleado(e, dbN.GetNegocio(e.Id_Negocio));
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(empleado);
            }
        }

        // GET: Empleado/Edit/5
        public ActionResult Edit(int id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var empleado = db.GetEmpleado(id);

            if(empleado == null)
            {
                return HttpNotFound();
            }
            return View(new EmpleadoViewModel(empleado));
        }

        // POST: Empleado/Edit/5
        [HttpPost]
        public ActionResult Edit(EmpleadoViewModel empleado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var e = empleado.aEmpleado();
                    db.UpdateEmpleado(e);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Empleado/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var empleado = db.GetEmpleado(id);

            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(new EmpleadoViewModel(empleado));
        }

        // POST: Empleado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var empleado = db.GetEmpleado(id);
            db.RemoveEmpleado(empleado);
            return RedirectToAction("Index");

        }
    }
}
