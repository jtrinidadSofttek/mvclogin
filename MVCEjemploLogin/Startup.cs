﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Models;
using MVCEjemploLogin.Models;
using Owin;

[assembly: OwinStartupAttribute(typeof(MVCEjemploLogin.Startup))]
namespace MVCEjemploLogin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
   
        }

        // In this method we will create default User roles and Admin user for login    
      
    }
}
