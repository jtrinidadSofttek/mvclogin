﻿CREATE TABLE [dbo].[Negocio] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]    NCHAR (50)    NOT NULL,
    [Email]     NVARCHAR (50) NOT NULL,
    [Direccion] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Negocio] PRIMARY KEY CLUSTERED ([Id] ASC)
);

