﻿CREATE TABLE [dbo].[Empleado] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]     NVARCHAR (50) NOT NULL,
    [Dni]        INT           NOT NULL,
    [Id_Negocio] INT           NOT NULL,
    [Sueldo]     FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_Empleado_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Empleado_Negocio] FOREIGN KEY ([Id_Negocio]) REFERENCES [dbo].[Negocio] ([Id])
);

