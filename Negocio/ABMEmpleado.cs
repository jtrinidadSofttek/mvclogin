﻿
using System.Collections.Generic;

using AccesoADatos;
using Models;


namespace NegocioBusiness
{
    public class ABMEmpleado
    {
        private Repository rp { get; }

        public ABMEmpleado() => rp = new Repository();

        public List<Empleado> GetEmpleados() => rp.GetEmpleados();

        public List<Empleado> GetEmpleados(int id) => rp.GetEmpleados(id);

        public Empleado GetEmpleado(int id) => rp.GetEmpleado(id);

        public void AddEmpleado(Empleado e, Models.Negocio n) => rp.AddEmpleado(n, e);

        public void RemoveEmpleado(Empleado e) => rp.RemoveEmpleado(e);

        public void UpdateEmpleado(Empleado e) => rp.UpdateEmpleado(e);
    }
}
