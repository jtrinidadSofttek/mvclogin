﻿using System.Collections.Generic;
using System.Linq;
using AccesoADatos;
using Models;

namespace NegocioBusiness
{
    public class ABMNegocio
    {
        private Repository rp { get; }

        public ABMNegocio() => rp = new Repository();
        
        public List<Models.Negocio> GetNegocios() => rp.GetNegocios();

        public Models.Negocio GetNegocio(int id) => rp.GetNegocio(id);

        public void AddNegocio(Models.Negocio n) => rp.AddNegocio(n);

        public void RemoveNegocio(Models.Negocio n) => rp.RemoveNegocio(n);

        public void UpdateNegocio(Models.Negocio n) => rp.UpdateNegocio(n);

        public List<Empleado> FilterNegocio(int id) => new ABMEmpleado().GetEmpleados().Where(x => x.Id_Negocio == id).ToList();
    }
}
