﻿using Models;
using NegocioBusiness;
using System.Collections.Generic;
using System.Data.Common;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NegocioController : ApiController
    {
      
        // GET: api/Negocio
        [HttpGet]
        public IHttpActionResult Get()
        {
            if(ModelState.IsValid)
            {
                List<NegocioDto> negocios = new List<NegocioDto>();
                new ABMNegocio().GetNegocios().ForEach(x => negocios.Add(new NegocioDto(x)));

                return Ok(negocios);
            }
            else
            {
                return NotFound();
            }   
        
        }

        // GET: api/Negocio/5
        public IHttpActionResult Get(int id)
        {
            Negocio n = new ABMNegocio().GetNegocio(id);
            if (n != null)
            {
                NegocioDto negocio = new NegocioDto(n);
                return Ok(negocio);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Negocio
        [HttpPost]
        public IHttpActionResult Post([FromBody] NegocioDto negocio)
        {
            if (ModelState.IsValid)
            {
                Negocio n = negocio.aNegocio();
                new ABMNegocio().AddNegocio(n);
                return Created("Creado con exito", negocio);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        // PUT: api/Negocio/5
        public IHttpActionResult Put(int id, [FromBody]NegocioDto negocio)
        {
            if (ModelState.IsValid)
            {
                Negocio n = negocio.aNegocio();
                new ABMNegocio().UpdateNegocio(n);
                return Ok(n);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        // DELETE: api/Negocio/5
        public IHttpActionResult Delete(int id)
        {
            Negocio n = new ABMNegocio().GetNegocio(id);
            if(n != null)
            {
                new ABMNegocio().RemoveNegocio(n);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
