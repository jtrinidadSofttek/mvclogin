﻿using Models;
using NegocioBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class EmpleadoController : ApiController
    {
        // GET: api/Empleado
        [HttpGet]
        public IHttpActionResult Get()
        {
            if (ModelState.IsValid)
            {
                List<EmpleadoDto> empleados = new List<EmpleadoDto>();
                new ABMEmpleado().GetEmpleados().ForEach(x => empleados.Add(new EmpleadoDto(x)));
                return Ok(empleados);
            }
            else
            {
                return BadRequest();
            }
        }

        // GET: api/Empleado/5
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
           Empleado em =  new ABMEmpleado().GetEmpleado(id);
            if (em != null)
            {
                EmpleadoDto empleado = new EmpleadoDto(new ABMEmpleado().GetEmpleado(id));
                return Ok(empleado);
            }
            else
            {
                return BadRequest();
            }
        }

        // POST: api/Empleado
        public IHttpActionResult Post([FromBody] EmpleadoDto empleado)
        {
            if (ModelState.IsValid)
            {
                Empleado e = empleado.aEmpleado();
                new ABMEmpleado().AddEmpleado(e, new ABMNegocio().GetNegocio(e.Id_Negocio));
                return Created("Creado con exito", new EmpleadoDto(e));
            }
            else
            {
                return BadRequest();
            }
        }

        // PUT: api/Empleado/5
        public IHttpActionResult Put(int id, [FromBody]EmpleadoDto empleado)
        {
            if (ModelState.IsValid)
            {
                Empleado e = empleado.aEmpleado();
                new ABMEmpleado().UpdateEmpleado(e);
                return Ok(e);
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE: api/Empleado/5
        public IHttpActionResult Delete(int id)
        {
            Empleado e = new ABMEmpleado().GetEmpleado(id);
            if (e != null)
            {
                new ABMEmpleado().UpdateEmpleado(e);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
