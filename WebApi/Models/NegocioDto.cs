﻿using Models;
using NegocioBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class NegocioDto
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Email { get; set; }

        public string Direccion { get; set; }

        public List<int> idEmpleado { get; set; }

        public NegocioDto() { }

        public NegocioDto(Negocio negocio)
        {
            Nombre = negocio.Nombre;
            Id = negocio.Id;
            Email = negocio.Email;
            Direccion = negocio.Direccion;
            idEmpleado = new List<int>();
            if (negocio.Empleado.Count > 0)
            {
                foreach (var item in negocio.Empleado)
                {
                    idEmpleado.Add(item.Id);
                }
            }

        }

        public Negocio aNegocio()
        {
            Negocio n = new Negocio();
            n.Nombre = Nombre;
            n.Direccion = Direccion;
            n.Id = Id;
            n.Email = Email; 
            idEmpleado = new List<int>();
            if (idEmpleado.Count > 0)
            {
                foreach (var item in idEmpleado)
                {
                    n.Empleado.Add(new ABMEmpleado().GetEmpleado(item));
                }
            }
            return n;

        }
    }

}
