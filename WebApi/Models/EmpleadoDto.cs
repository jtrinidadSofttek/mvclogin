﻿using Models;
using NegocioBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class EmpleadoDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Dni { get; set; }
        public int Id_Negocio { get; set; }
        public double Sueldo { get; set; }

        public EmpleadoDto() { }

        public EmpleadoDto(Empleado e)
        {
            Id = e.Id;
            Nombre = e.Nombre;
            Dni = e.Dni;
            Id_Negocio = e.Id_Negocio;
            Sueldo = e.Sueldo;
        }

        public Empleado aEmpleado()
        {
            Empleado e = new Empleado();
            e.Id = Id;
            e.Nombre = Nombre;
            e.Id_Negocio = Id_Negocio;
            e.Sueldo = Sueldo;
            e.Dni = Dni;
            //e.Negocio = new ABMNegocio().GetNegocio(e.Id_Negocio);

            return e;
        }
    }
}