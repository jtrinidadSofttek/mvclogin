﻿using Microsoft.AspNet.Identity.EntityFramework;
using Models;


namespace AccesoADatos
{
    public class ApplicationDbContext : IdentityDbContext <ApplicationUser>
    {
        public ApplicationDbContext()
            : base("CarniceriaEntitiesMVC")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}
